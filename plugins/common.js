import Vue from 'vue';
import { mapFields } from 'vuex-map-fields';
import numeral from 'numeral';
import md5 from 'md5';
import frag from 'vue-frag';
import XLSX from 'xlsx';
import objectFlat from '~/components/objectFlat.js';
import { getProfile } from '~/api/user.js';
Vue.directive('frag', frag);
Vue.mixin({
  computed: {
    ...mapFields('lang', ['langTranslate', 'langKey', 'selectLang']),
    ...mapFields('export', ['exportData', 'exportTotal'])
  },
  mounted () {

  },
  methods: {
    confirmWindow (message, ok, cancel) {
      this.$root.$emit('bv::show::modal', 'system-message');
      this.$store.dispatch('message/setMessage', {
        message,
        ok: async () => {
          if (ok) {
            const result = await ok();
            if (!result) {
              this.$root.$emit('bv::hide::modal', 'system-message');
            }
          } else {
            this.$root.$emit('bv::hide::modal', 'system-message');
          }
        },
        cancel: async () => {
          if (cancel) {
            const result = await cancel();
            if (!result) {
              this.$root.$emit('bv::hide::modal', 'system-message');
            }
          } else {
            this.$root.$emit('bv::hide::modal', 'system-message');
          }
        }
      });
    },
    alertWindow (message) {
      this.$root.$emit('bv::show::modal', 'system-message');
      this.$store.dispatch('message/setMessage', {
        message
      });
    },
    simpleDateFormat (data) {
      if (!data) {
        return;
      }
      const date = new Date(data);
      const year = date.getFullYear();
      const month = (date.getMonth() + 1).toString().padStart(2, 0);
      const day = date.getDate().toString().padStart(2, 0);
      const hour = date.getHours().toString().padStart(2, 0);
      const minute = date.getMinutes().toString().padStart(2, 0);
      const second = date.getSeconds().toString().padStart(2, 0);

      return [year, month, day].join('-') + ' ' + [hour, minute, second].join(':');
    },
    getLang (text) {
      const item = this.langTranslate[text];
      if (item) {
        if (item[this.selectLang]) {
          return item[this.selectLang];
        }
      }
      this.$store.dispatch('lang/setLangKey', text);
      return text;
    },
    getFilePath (image_id) {
      return this.$config.baseURL + md5(image_id + '').replace(/([\d\D]{3})([\d\D]+)/, '/upload/$1/$2');
    },
    async getProfile (callback) {
      try {
        const res = await getProfile();
        this.$store.dispatch('user/setGuide', res.guide);
        this.$store.dispatch('user/setProfile', res.data);
        this.$store.dispatch('user/setPower', res.power);
      } catch (err) {
        this.$store.dispatch('user/clearProfile');
        this.$router.push('/login');
        this.alertWindow(err);
      }
      callback && callback();
    },
    moneyFormat (string, format) {
      if (!format) {
        format = '0,0.00';
      }
      return numeral(string).format(format);
    },
    // getQueryToWhere () {
    //   const query = objectFlat.decode(this.$route.query);
    //   for (const field in query.where) {
    //     this.$set(this.where, field, query.where[field]);
    //   }
    // },
    // setQueryToWhere () {
    //   const query = JSON.parse(JSON.stringify(this.$route.query));
    //   const where = objectFlat.encode({ where: this.where });
    //   for (const field in where) {
    //     const item = where[field];
    //     if (item == -1 || item == '') {
    //       this.$delete(query, field);
    //     } else {
    //       this.$set(query, field, item);
    //     }
    //   }
    //   this.$router.push({ query });
    // },
    onexport (type = 'xlsx', routeName, tableHeader) {
      const headerData = [];
      const titleData = [];
      for (let i = 0; i < tableHeader.cells.length; i++) {
        headerData.push(String(i));
        titleData.push(tableHeader.cells[i].innerHTML);
      }
      let data = [...this.exportData];
      const totalData = this.exportTotal;
      data.unshift({ ...titleData });
      totalData.length ? data = data.concat(totalData) : data.push(totalData);
      const ws = XLSX.utils.json_to_sheet(data, { header: headerData, skipHeader: true });
      const wb = XLSX.utils.book_new();

      XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
      XLSX.writeFile(wb, `${routeName}.${type}`);
    },
    printData (id) {
      const printFrames = window.frames.printFrame;
      printFrames.document.body.outerHTML = document.getElementById(id).outerHTML;
      printFrames.window.focus();
      printFrames.window.print();
    },
    getStatusClass (status, type = 1) {
      switch (status) {
        case 0:
          return 'bg-warning';
        case 1:
          return 'bg-info';
        case 2:
          return 'bg-success';
        case 3:
          return 'bg-danger';
        case 4:
          return 'bg-danger';
        default:
          return 'bg-primary';
      }
    }
  }
});
Vue.filter('toFixed', function (value, point) {
  return (value * 1).toFixed(point || 2);
});
