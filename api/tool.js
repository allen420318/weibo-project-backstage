import { fetcher } from '~/assets/scripts/fetcher.js';

export const repeatIpAddress = async (params) => {
  const result = await fetcher('check_ip_address', 'GET', params);
  return result;
};

export const memberAccountIp = async (params) => {
  const result = await fetcher('check_Member', 'GET', params);
  return result;
};

export const getMemberDetail = async (params) => {
  const result = await fetcher('getMember', 'GET', params);
  return result;
};

export const getIpInfo = async (params) => {
  const result = await fetcher('getIpInfo', 'GET', params);
  return result;
};
