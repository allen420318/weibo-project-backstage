import { fetcher } from '~/assets/scripts/fetcher.js';

export default {
  async get (id) {
    const result = await fetcher('agentLink/get/' + id, 'GET');
    return result;
  },
  async set (params, id) {
    const result = await fetcher('agentLink/set/' + id, 'POST', {}, params);
    return result;
  },
  async del (id) {
    const result = await fetcher('agentLink/del/' + id, 'DELETE');
    return result;
  },
  async getHash () {
    const result = await fetcher('agentLink/getHash', 'GET');
    return result;
  }
};
