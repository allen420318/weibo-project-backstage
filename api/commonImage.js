import { fetcher } from '~/assets/scripts/fetcher.js';
export default {
  upload: async (data) => {
    const result = await fetcher('uploadImage', 'POST', {}, data);
    return result;
  },
  delete: async (data) => {
    const result = await fetcher('deleteImage', 'POST', {}, data);
    return result;
  },
  get: async (data) => {
    const result = await fetcher('image/get', 'POST', {}, data);
    return result;
  }
};
