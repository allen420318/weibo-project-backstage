import { fetcher } from '~/assets/scripts/fetcher.js';

export const login = async (data) => {
  const result = await fetcher('/login', 'POST', {}, data);
  return result;
};

export const getCaptcha = async () => {
  const result = await fetcher('/captcha/0', 'GET');
  return result;
};

export const getProfile = async () => {
  const result = await fetcher('/profile', 'GET');
  return result;
};

export const logout = async (data) => {
  const result = await fetcher('/logout', 'POST', {}, data);
  return result;
};
