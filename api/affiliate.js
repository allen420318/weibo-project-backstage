import { fetcher } from '~/assets/scripts/fetcher.js';

export const agentAdjustAmount = async (params) => {
  const result = await fetcher('AdjustmentAmount', 'GET', params);
  return result;
};

export const getAgentList = async (params) => {
  const result = await fetcher('affiliate/agent-management', 'GET', params);
  return result;
};

export const updateAgent = async (data, id) => {
  const result = await fetcher(`affiliate/agent-management/${id}`, 'PUT', {}, data);
  return result;
};

export const addAgent = async (data) => {
  const result = await fetcher('affiliate/agent-management', 'POST', {}, data);
  return result;
};

export const deleteAgent = async (id) => {
  const result = await fetcher(`affiliate/agent-management/${id}`, 'DELETE');
  return result;
};

export const getAgentdomain = async (params) => {
  const result = await fetcher('affiliate/agent_domain', 'GET', params);
  return result;
};

export const updateAgentdomain = async (data, id) => {
  const result = await fetcher(`affiliate/agent_domain/${id}`, 'PUT', {}, data);
  return result;
};

export const addAgentdomain = async (data) => {
  const result = await fetcher('affiliate/agent_domain', 'POST', {}, data);
  return result;
};

export const deleteAgentdomain = async (id) => {
  const result = await fetcher(`affiliate/agent_domain/${id}`, 'DELETE');
  return result;
};

export const chAgentManagement = async (account) => {
  const result = await fetcher(`chAgentManagement/${account}`, 'DELETE');
  return result;
};

export const getWalletsLog = async (params) => {
  const result = await fetcher('walletsLog', 'GET', params);
  return result;
};

export const getCommission = async (params) => {
  const result = await fetcher('getCommission', 'GET', params);
  return result;
};

export const getPerformance = async (params) => {
  const result = await fetcher('AgentPerformance', 'GET', params);
  return result;
};
