import { fetcher } from '~/assets/scripts/fetcher.js';

export const getPaymentList = async () => {
  const result = await fetcher('/getPayTypeList', 'GET');
  return result;
};

export const getPromoList = async (params) => {
  const result = await fetcher('/getPromoList', 'GET', params);
  return result;
};

export const depositOrder = async (data) => {
  const result = await fetcher('/generateOrder/JP', 'POST', {}, data);
  return result;
};

export const getRecordList = async (params) => {
  const result = await fetcher('/order', 'GET', params);
  return result;
};

export const withdrawal = async (data) => {
  const result = await fetcher('/withdrawal', 'POST', {}, data);
  return result;
};
