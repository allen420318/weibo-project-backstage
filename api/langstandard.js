import { fetcher } from '~/assets/scripts/fetcher.js';

export const getLanguageList = async (params) => {
  const result = await fetcher('lang/Langstandard', 'GET', params);
  return result;
};

export const addLanguage = async (data) => {
  const result = await fetcher('lang/Langstandard', 'POST', {}, data);
  return result;
};

export const updateLanguage = async (data, id) => {
  const result = await fetcher(`lang/Langstandard/${id}`, 'PUT', {}, data);
  return result;
};

export const deleteLanguage = async (id) => {
  const result = await fetcher(`lang/Langstandard/${id}`, 'DELETE');
  return result;
};

export const langTranslation = async (current, target) => {
  const result = await fetcher(`langeTranslation/${current.lang}/${target.lang}`, 'POST', {}, current);
  return result;
};

export const getTranslationList = async (params) => {
  const result = await fetcher('Langtranslation', 'GET', params);
  return result;
};

export const updateTranslation = async (data, id) => {
  const result = await fetcher(`Langtranslation/${id}`, 'PUT', {}, data);
  return result;
};

export const deleteTranslation = async (id) => {
  const result = await fetcher(`Langtranslation/${id}`, 'DELETE');
  return result;
};

export const addTranslation = async (data) => {
  const result = await fetcher('Langtranslation', 'POST', {}, data);
  return result;
};

export const getLanguageData = async (language) => {
  const result = await fetcher(`language/${language}`, 'GET');
  return result;
};

export const reimportLang = async (language) => {
  const result = await fetcher(`restatrlang/${language}`, 'GET');
  return result;
};
