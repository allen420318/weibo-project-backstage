import { fetcher } from '~/assets/scripts/fetcher.js';

export const getWashCodeList = async (params) => {
  const result = await fetcher('wash-code/wash-code', 'GET', params);
  return result;
};

export const addWashCode = async (data) => {
  const result = await fetcher('wash-code/wash-modify', 'POST', {}, data);
  return result;
};

export const getMemberID = async (id) => {
  const result = await fetcher(`wash-code/wash-code/${id}`, 'GET');
  return result;
};

export const resetFlow = async (id) => {
  const result = await fetcher(`wash-code/wash-code/${id}`, 'GET');
  return result;
};

export const updateWashCode = async (data, id) => {
  const result = await fetcher(`wash-code/wash-code/${id}`, 'PUT', {}, data);
  return result;
};
