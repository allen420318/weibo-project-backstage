import { fetcher } from '~/assets/scripts/fetcher.js';

export const getRoleGuide = async (id) => {
  const result = await fetcher(`role-guide/${id}`, 'GET');
  return result;
};

export const setRoleGuide = async (data) => {
  const result = await fetcher('role-guide', 'POST', {}, data);
  return result;
};
