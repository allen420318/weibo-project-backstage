import { fetcher } from '~/assets/scripts/fetcher.js';

export const getMemberGroupList = async (params) => {
  const result = await fetcher('member/member-group', 'GET', params);
  return result;
};

export const updateMemberGroup = async (data, id) => {
  const result = await fetcher(`member/member-group/${id}`, 'PUT', {}, data);
  return result;
};

export const addMemberGroup = async (data) => {
  const result = await fetcher('member/member-group', 'POST', {}, data);
  return result;
};

export const deleteMemberGroup = async (id) => {
  const result = await fetcher(`member/member-group/${id}`, 'DELETE');
  return result;
};

export const getMemberList = async (params) => {
  const result = await fetcher('member/member-listing', 'GET', params);
  return result;
};

export const updateMember = async (data, id) => {
  const result = await fetcher(`member/member-listing/${id}`, 'PUT', {}, data);
  return result;
};

export const addMember = async (data) => {
  const result = await fetcher('member/member-listing', 'POST', {}, data);
  return result;
};

export const deleteMember = async (id) => {
  const result = await fetcher(`member/member-listing/${id}`, 'DELETE');
  return result;
};

export const getOnlineUser = async (params) => {
  const result = await fetcher('getOnlineUser', 'GET', params);
  return result;
};

export const kickUser = async (id) => {
  const result = await fetcher(`kickUser/${id}`, 'DELETE');
  return result;
};

export const sendSms = async (data) => {
  const result = await fetcher('member/sendSMS', 'POST', {}, data);
  return result;
};

export const Adjustment = async (data) => {
  const result = await fetcher('Adjustment', 'POST', {}, data);
  return result;
};

// export const sendInbox = async (data) => {
//   const result = await fetcher('inbox', 'POST', {}, data);
//   return result;
// };

export const changeAgent = async (data) => {
  const result = await fetcher('member/changeAgent', 'POST', {}, data);
  return result;
};
export const changeGroup = async (data) => {
  const result = await fetcher('member/changeGroup', 'POST', {}, data);
  return result;
};
export const lockProduct = async (data) => {
  const result = await fetcher('member/lockProduct', 'POST', {}, data);
  return result;
};

export const sendMessage = async (data) => {
  const result = await fetcher('member/sendMessage', 'POST', {}, data);
  return result;
};
