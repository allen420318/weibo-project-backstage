import { fetcher } from '~/assets/scripts/fetcher.js';

export const getReport = async (params) => {
  const result = await fetcher('report/report', 'GET', params);
  return result;
};

export const getMemberReport = async (params) => {
  const result = await fetcher('report/member-report', 'GET', params);
  return result;
};

export const getWinloseReport = async (params) => {
  const result = await fetcher('winlose', 'GET', params);
  return result;
};
