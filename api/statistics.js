import { fetcher } from '~/assets/scripts/fetcher.js';

export const get = async (data) => {
  const result = await fetcher('statistics', 'POST', {}, data);
  return result;
};

export const getGameStatistic = async (param) => {
  const result = await fetcher('winlose', 'GET', param);
  return result;
};

export const getWebStatistic = async (param) => {
  const result = await fetcher('webStatistic', 'GET', param);
  return result;
};

export const getMemberWinloseStatistic = async (param) => {
  const result = await fetcher('memberStatistic_1_and_2', 'GET', param);
  return result;
};

export const getMemberNotOperateStatistic = async (param) => {
  const result = await fetcher('memberStatistic_3_and_4', 'GET', param);
  return result;
};

export const getMemberTransactionStatistic = async (param) => {
  const result = await fetcher('memberStatistic_5_and_6_and_7', 'GET', param);
  return result;
};

export const getTransactionStatistic = async (param) => {
  const result = await fetcher('Transaction_statistics', 'GET', param);
  return result;
};

export const getPlayerFrequency = async (param) => {
  const result = await fetcher('Player_frequency', 'GET', param);
  return result;
};

export const getInactiveCustomers = async (param) => {
  const result = await fetcher('Inactive_customers', 'GET', param);
  return result;
};
