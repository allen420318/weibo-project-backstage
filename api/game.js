import { fetcher } from '~/assets/scripts/fetcher.js';

export const getBalance = async (data) => {
  const result = await fetcher('/game/GetBalance', 'POST', data);
  return result;
};

export const transport = async (data) => {
  const result = await fetcher('/game/transport', 'POST', data);
  return result;
};

export const openGame = async (data) => {
  const result = await fetcher('/game/openGame', 'POST', data);
  return result;
};

export const allPoint = async (data) => {
  const result = await fetcher('/game/AllPoint', 'POST', data);
  return result;
};

export const AtoBpoint = async (data) => {
  const result = await fetcher('/game/AtoBpoint', 'POST', data);
  return result;
};
