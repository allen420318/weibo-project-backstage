import { fetcher } from '~/assets/scripts/fetcher.js';

export const getList = async (payload) => {
  const result = await fetcher('order', 'GET', payload);
  return result;
};
export const getItem = async (id) => {
  const result = await fetcher(`order/${id}`, 'GET');
  return result;
};
export const update = async (payload, id) => {
  const result = await fetcher(`order/${id}`, 'PUT', payload);
  return result;
};
export const del = async (id) => {
  const result = await fetcher(`order/${id}`, 'DELETE');
  return result;
};
export const insert = async (payload) => {
  const result = await fetcher('order', 'POST', payload);
  return result;
};
export const fixItem = async (id) => {
  const result = await fetcher(`fixOrder/${id}`, 'POST');
  return result;
};
export const deal = async (payload, id) => {
  const result = await fetcher(`addOrder/${id}`, 'POST', payload);
  return result;
};
export const cancelOrder = async (id) => {
  const result = await fetcher(`cancelOrder/${id}`, 'POST');
  return result;
};
