import { fetcher } from '~/assets/scripts/fetcher.js';

export const getLangList = async (params) => {
  const result = await fetcher('lang/lang', 'GET', params);
  return result;
};

export const addLang = async (data) => {
  const result = await fetcher('lang/lang', 'POST', {}, data);
  return result;
};

export const updateLang = async (data, id) => {
  const result = await fetcher(`lang/lang/${id}`, 'PUT', {}, data);
  return result;
};

export const deleteLang = async (id) => {
  const result = await fetcher(`lang/lang/${id}`, 'DELETE');
  return result;
};

export const setSessionLang = async (lang) => {
  const result = await fetcher(`sessionLang/${lang}`, 'POST');
  return result;
};
export const getTransList = async (payload) => {
  const result = await fetcher('getTransList', 'POST', {}, payload);
  return result;
};
