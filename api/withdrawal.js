import { fetcher } from '~/assets/scripts/fetcher.js';

export const getWithdrawal = async (params) => {
  const result = await fetcher('withdrawal/get', 'GET', params);
  return result;
};
export const reviewWithdrawal = async (params) => {
  const result = await fetcher('withdrawal/review', 'POST', {}, params);
  return result;
};
