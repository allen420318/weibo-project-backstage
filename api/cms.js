import { fetcher } from '~/assets/scripts/fetcher.js';

export const getAnnouncementList = async (params) => {
  const result = await fetcher('cms/announcement', 'GET', params);
  return result;
};

export const updateAnnouncement = async (data, id) => {
  const result = await fetcher(`cms/announcement/${id}`, 'PUT', {}, data);
  return result;
};

export const addAnnouncement = async (data) => {
  const result = await fetcher('cms/announcement', 'POST', {}, data);
  return result;
};

export const deleteAnnouncement = async (id) => {
  const result = await fetcher(`cms/announcement/${id}`, 'DELETE');
  return result;
};

export const getPromotionList = async (params) => {
  const result = await fetcher('cms/promotion-management', 'GET', params);
  return result;
};

export const updatePromotion = async (data, id) => {
  const result = await fetcher(`cms/promotion-management/${id}`, 'PUT', {}, data);
  return result;
};

export const addPromotion = async (data) => {
  const result = await fetcher('cms/promotion-management', 'POST', {}, data);
  return result;
};

export const deletePromotion = async (id) => {
  const result = await fetcher(`cms/promotion-management/${id}`, 'DELETE');
  return result;
};
