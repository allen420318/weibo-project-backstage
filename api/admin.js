import { fetcher } from '~/assets/scripts/fetcher.js';

export const getGuideList = async () => {
  const result = await fetcher('admin/guide-management', 'GET');
  return result;
};

export const getRoleList = async (params) => {
  const result = await fetcher('admin/role-management', 'GET', params);
  return result;
};

export const getAccountList = async (params) => {
  const result = await fetcher('admin/account-management', 'GET', params);
  return result;
};

export const deleteAccount = async (id) => {
  const result = await fetcher(`admin/account-management/${id}`, 'DELETE');
  return result;
};

export const addAccount = async (data) => {
  const result = await fetcher('admin/account-management', 'POST', {}, data);
  return result;
};

export const updateAccount = async (data, id) => {
  const result = await fetcher(`admin/account-management/${id}`, 'PUT', {}, data);
  return result;
};

export const deleteRole = async (id) => {
  const result = await fetcher(`admin/role-management/${id}`, 'DELETE');
  return result;
};

export const addRole = async (data) => {
  const result = await fetcher('admin/role-management', 'POST', {}, data);
  return result;
};

export const updateRole = async (data, id) => {
  const result = await fetcher(`admin/role-management/${id}`, 'PUT', {}, data);
  return result;
};

export const deleteGuide = async (id) => {
  const result = await fetcher(`admin/guide-management/${id}`, 'DELETE');
  return result;
};

export const addGuide = async (data) => {
  const result = await fetcher('admin/guide-management', 'POST', {}, data);
  return result;
};

export const updateGuide = async (data, id) => {
  const result = await fetcher(`admin/guide-management/${id}`, 'PUT', {}, data);
  return result;
};

export const getCommonParent = async (params) => {
  const result = await fetcher('admin/common-parent', 'GET', params);
  return result;
};

export const addCommonParent = async (data) => {
  const result = await fetcher('admin/common-parent', 'POST', {}, data);
  delete data.type;
  return result;
};
export const updateCommonParent = async (data) => {
  const result = await fetcher('admin/common-parent/put', 'PUT', {}, data);
  delete data.type;
  return result;
};
export const delCommonParent = async (data) => {
  const result = await fetcher('admin/common-parent/delete', 'DELETE', {}, data);
  delete data.type;
  return result;
};

export const addRouteGuide = async (data) => {
  const result = await fetcher('guide', 'POST', {}, data);
  return result;
};
export const getRouteGuideList = async () => {
  const result = await fetcher('guide', 'GET');
  return result;
};
export const updateRouteGuide = async (data, id) => {
  const result = await fetcher(`guide/${id}`, 'PUT', {}, data);
  return result;
};
export const deleteRouteGuide = async (id) => {
  const result = await fetcher(`guide/${id}`, 'DELETE');
  return result;
};
