import { fetcher } from '~/assets/scripts/fetcher.js';

export const getBankList = async (params) => {
  const result = await fetcher('bank/banks', 'GET', params);
  return result;
};

export const updateBank = async (data, id) => {
  const result = await fetcher(`bank/banks/${id}`, 'PUT', {}, data);
  return result;
};

export const addBank = async (data) => {
  const result = await fetcher('bank/banks', 'POST', {}, data);
  return result;
};

export const deleteBank = async (id) => {
  const result = await fetcher(`bank/banks/${id}`, 'DELETE');
  return result;
};

export const getBankAccountList = async (params) => {
  const result = await fetcher('bank/bank-accounts', 'GET', params);
  return result;
};

export const updateBankAccount = async (data, id) => {
  const result = await fetcher(`bank/bank-accounts/${id}`, 'PUT', {}, data);
  return result;
};

export const addBankAccount = async (data) => {
  const result = await fetcher('bank/bank-accounts', 'POST', {}, data);
  return result;
};

export const deleteBankAccount = async (id) => {
  const result = await fetcher(`bank/bank-accounts/${id}`, 'DELETE');
  return result;
};

export const getBankLevelList = async (params) => {
  const result = await fetcher('bank/bank-level', 'GET', params);
  return result;
};

export const updateBankLevel = async (data, id) => {
  const result = await fetcher(`bank/bank-level/${id}`, 'PUT', {}, data);
  return result;
};

export const addBankLevel = async (data) => {
  const result = await fetcher('bank/bank-level', 'POST', {}, data);
  return result;
};

export const deleteBankLevel = async (id) => {
  const result = await fetcher(`bank/bank-level/${id}`, 'DELETE');
  return result;
};

// export const getBankAccount = async (id) => {
//   const result = await fetcher(`getBankAccount/${id}`, 'GET');
//   return result;
// };

export const fundTransfer = async (data) => {
  const result = await fetcher('bank_A_to_B', 'POST', {}, data);
  return result;
};

export const fundInOut = async (data) => {
  const result = await fetcher('bank_A_todo', 'POST', {}, data);
  return result;
};
