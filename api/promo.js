import { fetcher } from '~/assets/scripts/fetcher.js';

export const getPromoList = async (params) => {
  const result = await fetcher('promo/promo', 'GET', params);
  return result;
};

export const updatePromo = async (data, id) => {
  const result = await fetcher(`promo/promo/${id}`, 'PUT', {}, data);
  return result;
};

export const addPromo = async (data) => {
  const result = await fetcher('promo/promo', 'POST', {}, data);
  return result;
};

export const deletePromo = async (id) => {
  const result = await fetcher(`promo/promo/${id}`, 'DELETE');
  return result;
};

export const getPromoReportList = async (params) => {
  const result = await fetcher('promo/promo-report', 'GET', params);
  return result;
};

export const getTicketList = async (params) => {
  const result = await fetcher('getPromoReportTicketList', 'GET', params);
  return result;
};

export const getPromoTypeList = async (params) => {
  const result = await fetcher('promo/promo-type', 'GET', params);
  return result;
};

export const updatePromoType = async (data, id) => {
  const result = await fetcher(`promo/promo-type/${id}`, 'PUT', {}, data);
  return result;
};

export const addPromoType = async (data) => {
  const result = await fetcher('promo/promo-type', 'POST', {}, data);
  return result;
};

export const deletePromoType = async (id) => {
  const result = await fetcher(`promo/promo-type/${id}`, 'DELETE');
  return result;
};

// Rebate
export const getRebate = async (params) => {
  const result = await fetcher('promo/rebate', 'GET', params);
  return result;
};

export const getMemberGroupRebate = async (id) => {
  const result = await fetcher(`promo/rebate/${id}`, 'GET');
  return result;
};

export const addRebate = async (data) => {
  const result = await fetcher('promo/rebate', 'POST', {}, data);
  return result;
};

export const updateRebate = async (data, id) => {
  const result = await fetcher(`promo/rebate/${id}`, 'PUT', {}, data);
  return result;
};

export const getMemberRebate = async (params) => {
  const result = await fetcher('promo/rebate-member', 'GET', params);
  return result;
};

export const getReportRebate = async (params) => {
  const result = await fetcher('promo/rebate-report', 'GET', params);
  return result;
};

export const getRebateMemberList = async (params) => {
  const result = await fetcher('promo/rebate-member', 'GET', params);
  return result;
};

export const updateMemberRebate = async (data, id) => {
  const result = await fetcher(`promo/rebate-member/${id}`, 'PUT', {}, data);
  return result;
};

export const getRebateRecordList = async (params) => {
  const result = await fetcher('promo/rebate-record', 'GET', params);
  return result;
};
