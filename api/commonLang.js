import { fetcher } from '~/assets/scripts/fetcher.js';
export default {
  get: async (payload) => {
    const result = await fetcher('commonLang/get', 'POST', {}, payload);
    return result;
  },
  set: async (payload) => {
    const result = await fetcher('commonLang/set', 'POST', {}, payload);
    return result;
  },
  del: async (payload) => {
    const result = await fetcher('commonLang/del', 'POST', {}, payload);
    return result;
  }
};
