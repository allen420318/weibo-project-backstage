import { fetcher } from '~/assets/scripts/fetcher.js';

export const get = async (data) => {
  const result = await fetcher('powerGuide/get', 'POST', {}, data);
  return result;
};
export const set = async (data) => {
  const result = await fetcher('powerGuide/set', 'POST', {}, data);
  return result;
};
export const del = async (data) => {
  const result = await fetcher('powerGuide/del', 'POST', {}, data);
  return result;
};
