import { fetcher } from '~/assets/scripts/fetcher.js';

export const getProductList = async (params) => {
  const result = await fetcher('product/product-main', 'GET', params);
  return result;
};

export const updateProduct = async (data, id) => {
  const result = await fetcher(`product/product-main/${id}`, 'PUT', {}, data);
  return result;
};

export const addProduct = async (data) => {
  const result = await fetcher('product/product-main', 'POST', {}, data);
  return result;
};

export const deleteProduct = async (id) => {
  const result = await fetcher(`product/product-main/${id}`, 'DELETE');
  return result;
};

export const getGameTypeList = async (params) => {
  const result = await fetcher('product/game-type', 'GET', params);
  return result;
};

export const addGameType = async (data) => {
  const result = await fetcher('product/game-type', 'POST', {}, data);
  return result;
};

export const updateGameType = async (data, id) => {
  const result = await fetcher(`updateGameTypeItem/${id}`, 'POST', {}, data);
  return result;
};

export const deleteGameType = async (id) => {
  const result = await fetcher(`product/game-type/${id}`, 'DELETE');
  return result;
};
