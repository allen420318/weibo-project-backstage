import { fetcher } from '~/assets/scripts/fetcher.js';

export const getCountryList = async (params) => {
  const result = await fetcher('lang/country', 'GET', params);
  return result;
};

export const addCountry = async (data) => {
  const result = await fetcher('lang/country', 'POST', {}, data);
  return result;
};

export const updateCountry = async (data, id) => {
  const result = await fetcher(`lang/country/${id}`, 'PUT', {}, data);
  return result;
};

export const deleteCountry = async (id) => {
  const result = await fetcher(`lang/country/${id}`, 'DELETE');
  return result;
};
// export const getCurrentCountry = async () => {
//   const result = await fetcher('getCountry', 'GET');
//   return result;
// };
