import { fetcher } from '~/assets/scripts/fetcher.js';

export const transABLogInsert = async (data) => {
  const result = await fetcher('/transABLog', 'POST', data);
  return result;
};

export const transABLogList = async (data) => {
  const result = await fetcher('/transABLog', 'GET', data);
  return result;
};
