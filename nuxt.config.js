const isDev = process.env.NODE_ENV === 'development';
export default {
  /*
  ** Nuxt rendering mode
  ** See https://nuxtjs.org/api/configuration-mode
  */
  ssr: false,
  // Target (https://go.nuxtjs.dev/config-target)
  target: 'static',

  // Global page headers (https://go.nuxtjs.dev/config-head)
  head: {
    title: '微博帝國',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: '/assets/css/app.min.css' },
      // { rel: 'stylesheet', href: '/assets/bundles/datatables/datatables.min.css' },
      // { rel: 'stylesheet', href: '/assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css' },
      { rel: 'stylesheet', href: '/assets/css/style.css' },
      { rel: 'stylesheet', href: '/assets/css/components.css' }
    ]
  },

  // Global CSS (https://go.nuxtjs.dev/config-css)
  css: [
  ],

  // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
  plugins: [
    { src: '~/plugins/fragment.js' },
    { src: '~/plugins/common.js' },
    { src: '~/plugins/vue2-datepicker.js' }
  ],

  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: true,

  // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module'
  ],

  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    'cookie-universal-nuxt',
    'bootstrap-vue/nuxt'
  ],
  bootstrapVue: {
    bootstrapCSS: false // Or `css: false`
    // bootstrapVueCSS: false // Or `bvCSS: false`
  },
  // Axios module configuration (https://go.nuxtjs.dev/config-axios)
  axios: {
  },
  env: {
    baseUrl: (process.env.baseURL || '') + '/api/'
  },
  server: {
    port: process.env.PORT || 4000, // default: 3000
    host: '0.0.0.0' // default: localhost
  },
  // Build Configuration (https://go.nuxtjs.dev/config-build)
  build: {
    parallel: isDev,
    hardSource: isDev,
    cache: isDev,
    babel: {
      compact: true
    }
  },
  publicRuntimeConfig: {
    baseURL: process.env.baseURL || ''
  }
};
