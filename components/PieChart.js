import { Pie, mixins } from 'vue-chartjs';
const { reactiveProp } = mixins;

export default {
  extends: Pie,
  mixins: [reactiveProp],
  props: ['options'],
  data () {
    return {
      color: ['#00a65a', '#dd4b39', '#0073b7', '#f39c12', '#605ca8']
    };
  },
  mounted () {
    this.chartData.dateList.length > 5 && this.getRandomColor();
    this.getChart();
  },
  methods: {
    getChart () {
      const chartData = {
        labels: this.chartData.dateList,
        datasets: [{
          label: 'Data',
          backgroundColor: this.color,
          data: this.chartData.list
        }]
      };
      this.renderChart(chartData, this.options);
    },
    getRandomColor () {
      const letters = '0123456789ABCDEF'.split('');
      for (let i = 0; i < this.chartData.dateList.length - 5; i++) {
        let color = '#';
        for (let j = 0; j < 6; j++) {
          color += letters[Math.floor(Math.random() * 16)];
        }
        this.color.push(color);
      }
    }
  }
};
