import { Line, mixins } from 'vue-chartjs';
const { reactiveProp } = mixins;

export default {
  extends: Line,
  mixins: [reactiveProp],
  props: ['options'],
  data () {
    return {
      color: ['00a65a', 'dd4b39', '0073b7', 'f39c12', '605ca8', '000000']
    };
  },
  mounted () {
    // this.chartData 在 mixin 创建.
    // 如果你需要替换 options , 请创建本地的 options 对象

    const chartData = {
      labels: this.chartData.dateList,
      datasets: []
    };
    for (const i in this.chartData.list) {
      const item = this.chartData.list[i];
      const color = this.color[i];
      chartData.datasets.push({
        label: this.getLang(item.name),
        pointBackgroundColor: '#' + color,
        fill: false,
        borderColor: '#' + color,
        data: item.list
      });
    }
    this.renderChart(chartData, this.options);
  }
};
