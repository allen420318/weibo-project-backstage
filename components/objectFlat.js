const encode = function (list, key_name, result) {
  key_name || (key_name = '');
  result || (result = {});
  for (const i in list) {
    let appendKeyName = '';
    if (key_name == '') {
      appendKeyName = i;
    } else {
      appendKeyName = '[' + i + ']';
    }
    if (typeof list[i] == 'object') {
      encode(list[i], key_name + appendKeyName, result);
    } else {
      result[key_name + appendKeyName] = list[i];
    }
  }
  return result;
};
const decode = function (list, result) {
  result || (result = {});
  let index = -1;
  for (let field in list) {
    const keyList = [];
    const value = list[field];
    index = field.indexOf('[');
    if (index == -1) {
      keyList.push(field);
    } else {
      keyList.push(field.substr(0, index));
      field = field.substr(index);
      while (field) {
        index = field.indexOf(']');
        keyList.push(field.substr(1, index - 1));
        field = field.substr(index + 1);
      }
    }

    let tmpResult = result;
    for (let i = 0; i < keyList.length; i++) {
      const key_name = keyList[i];

      if (!tmpResult[key_name]) {
        if (keyList[i + 1] * 1 == keyList[i + 1]) {
          tmpResult[key_name] = [];
        } else {
          tmpResult[key_name] = {};
        }
      }
      if (keyList.length - 1 == i) {
        if (key_name * 1 == key_name) {
          tmpResult.push(value);
        } else {
          tmpResult[key_name] = value;
        }
      }
      tmpResult = tmpResult[key_name];
    }
  }

  return result;
};
export default {
  encode,
  decode
};
