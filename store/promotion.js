import { getField, updateField } from 'vuex-map-fields';

const statusList = [
  { show: '關閉', value: '0' },
  { show: '開啟', value: '1' }
];
// statusArray: [{ value: 1, show: '活躍' }, { value: 0, show: '非活躍' }],
const statusObject = {};
for (const i in statusList) {
  const item = statusList[i];
  statusObject[item.value] = item.show;
}

export const state = () => ({
  statusList,
  statusObject
});

export const mutations = {
  updateField,
  setData (state, payload) {
    state[payload.item] = payload.data;
  },
  resetData (state, payload) {
    state[payload.item] = null;
  }
};

export const actions = {

};

export const getters = {
  getField
};
