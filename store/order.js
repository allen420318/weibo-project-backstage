import { getField, updateField } from 'vuex-map-fields';
import { getProductList } from '~/api/product.js';
const updateTimeList = [
  { show: '5秒', value: 5 },
  { show: '10秒', value: 10 },
  { show: '30秒', value: 30 },
  { show: '60秒', value: 60 },
  { show: '暫停', value: -1 }
];
const typeList = [
  { show: '存款', value: '1' },
  { show: '提款', value: '2' },
  // { show: '遊戲轉帳', value: '3' },
  // { show: '代理轉貼', value: '4' },
  { show: '優惠', value: '5' }
  // { show: '額度調整', value: '6' }
];
const channelList = [
  { show: '網銀轉帳', value: '1' },
  { show: 'ATM轉帳', value: '2' },
  { show: '現金存款', value: '3' },
  { show: '支付網關', value: '4' },
  { show: '提款網關', value: '5' }
];
const statusList = [
  { show: '新要求', value: '0' },
  { show: '處理中', value: '1' },
  { show: '已被確認', value: '2' },
  { show: '已遭拒', value: '3' }
];

const channelObject = {};
for (const i in channelList) {
  const item = channelList[i];
  channelObject[item.value] = item.show;
}

const statusObject = {};
for (const i in statusList) {
  const item = statusList[i];
  statusObject[item.value] = item.show;
}

const typeObject = {};
for (const i in typeList) {
  const item = typeList[i];
  typeObject[item.value] = item.show;
}

export const state = () => ({
  updateTimeList,
  channelList,
  typeList,
  statusList,
  channelObject,
  statusObject,
  typeObject,
  productList: [],
  productObject: {},
  bankId: -1,
  bankAccountId: -1,
  fee: 0,
  note: '',
  tradeChannel: -1
});

export const mutations = {
  updateField,
  setData (state, payload) {
    state[payload.item] = payload.data;
  },
  resetData (state, payload) {
    state[payload.item] = null;
  }
};

export const actions = {
  async getProductList ({ commit, state }) {
    if (state.productList.length) { return; };
    const { data } = await getProductList({
      where: { type: 1 }
    });
    // data.unshift({ id: 0, name: '公司' });

    const productObject = {};
    for (const i in data) {
      const item = data[i];
      productObject[item.id] = item.name;
    }
    commit('setData', { item: 'productList', data });
    commit('setData', { item: 'productObject', data: productObject });
  }

};

export const getters = {
  getField
};
