import { getField, updateField } from 'vuex-map-fields';
import { getCountryList } from '~/api/country.js';
export const state = () => ({
  countryList: [],
  countryObject: {}
});

export const mutations = {
  updateField,
  setData (state, payload) {
    state[payload.item] = payload.data;
  }
};

export const actions = {
  setList ({ commit }, val) {
    getCountryList().then(({ data }) => {
      const countryList = [];
      const countryObject = {};
      for (const i in data) {
        const item = data[i];
        countryList.push(item);
        countryObject[item.id] = item.currency;
      }

      commit('setData', { item: 'countryList', data: countryList });
      commit('setData', { item: 'countryObject', data: countryObject });
    });
  }
};

export const getters = {
  getField
};
