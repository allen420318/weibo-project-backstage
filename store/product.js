import { getField, updateField } from 'vuex-map-fields';
import { getProductList } from '~/api/product.js';

const statusList = [
  { show: '成功', value: 1 },
  { show: '正在進行', value: 2 },
  { show: '被取消', value: 3 }
];

const productTypeList = [
  { show: '真人線上娛樂場', value: 1 },
  { show: '真人電子娛樂城', value: 2 },
  { show: '體育博彩', value: 3 },
  { show: '樂透', value: 4 },
  { show: '棋牌遊戲', value: 5 },
  { show: '撲克', value: 6 },
  { show: '打魚遊戲', value: 7 },
  { show: '電子競技', value: 8 },
  { show: '其他選項', value: 99 }
];

const statusObject = {};
for (const i in statusList) {
  const item = statusList[i];
  statusObject[item.value] = item.show;
}

const productTypeObject = {};
for (const i in productTypeList) {
  const item = productTypeList[i];
  productTypeObject[item.value] = item.show;
}

export const state = () => ({
  statusList,
  statusObject,
  productTypeList,
  productTypeObject,
  productList: [],
  productObject: {}
});

export const mutations = {
  updateField,
  setData (state, payload) {
    state[payload.item] = payload.data;
  },
  resetData (state, payload) {
    state[payload.item] = null;
  }
};

export const actions = {
  async initData ({ commit, state }) {
    if (state.productList.length) { return; };
    const { data } = await getProductList({
      where: { type: 1 }
    });

    const productObject = {};
    for (const i in data) {
      const item = data[i];
      productObject[item.id] = item.name;
    }
    commit('setData', { item: 'productList', data });
    commit('setData', { item: 'productObject', data: productObject });
  }

};

export const getters = {
  getField
};
