import { getField, updateField } from 'vuex-map-fields';
import { getMemberGroupList, getMemberList, deleteMember, addMember, updateMember } from '~/api/member.js';

export const state = () => ({
  list: [],
  object: {}
});

export const mutations = {
  updateField,
  setData (state, payload) {
    state[payload.item] = payload.data;
  },
  resetData (state, payload) {
    state[payload.item] = null;
  }
};

export const actions = {
  async getList ({ commit, state }) {
    const { data } = await getMemberGroupList();
    commit('setData', { item: 'list', data });
    const object = {};
    for (const i in data) {
      object[data[i].id] = data[i];
    }
    commit('setData', { item: 'object', data: object });
  }
};

export const getters = {
  getField
};
