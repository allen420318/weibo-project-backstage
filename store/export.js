import { getField, updateField } from 'vuex-map-fields';

export const state = () => ({
  exportData: [],
  exportTotal: null
});

export const mutations = {
  updateField,
  setData (state, payload) {
    state[payload.item] = payload.data;
  },
  resetData (state, payload) {
    state[payload.item] = null;
  }
};

export const actions = {
  'rebate-report-data' ({ commit }, val) {
    const result = val.map((x, index) => {
      const result = {
        0: index + 1,
        1: '帳號: ' + x.account + '\n日期: ' + x.dateRang + '\nGame: ' + x.product,
        2: x.amount,
        3: x.rebate
      };
      return result;
    });
    commit('setData', { item: 'exportData', data: result });
  },
  'transaction-report-data' ({ commit }, val) {
    const result = val.map((x, index) => {
      const result = {
        0: index + 1,
        1: x.account,
        2: x.AdjustmentPositiveAmount,
        3: x.AdjustmentNegativeAmount,
        4: x.PositiveAmount,
        5: x.NegativeAmount,
        6: x.DiscountAmount,
        7: '0.00'
      };
      return result;
    });
    commit('setData', { item: 'exportData', data: result });
  },
  'real-time-data' ({ commit }, val) {
    const result = val.map((x, index) => {
      const result = {
        0: index + 1,
        1: x.created_at,
        2: x.id,
        3: x.memberAccount,
        4: x.memberName || '--',
        5: x.member_groups || '--',
        6: x.email || '--',
        7: x.type,
        8: x.note || '--',
        9: '--',
        10: x.status,
        11: x.withdraw,
        12: x.deposit,
        13: x.handling_fee || '--',
        14: '--'
      };
      return result;
    });
    commit('setData', { item: 'exportData', data: result });
  },
  'off-rec-adj-hist-data' ({ commit }, val) {
    const result = val.map((x, index) => {
      const result = {
        0: index + 1,
        1: x.created_at,
        2: x.memberAccount,
        3: x.type,
        4: x.amount,
        5: x.before_wallet || '--',
        6: x.after_wallet || '--',
        7: x.operator_accounts || '--',
        8: x.note || '--'
      };
      return result;
    });
    commit('setData', { item: 'exportData', data: result });
  },
  'bank-report-data' ({ commit }, val) {
    const result = val.map((x, index) => {
      const result = {
        0: index + 1,
        1: x.bank_name,
        2: x.bank_acc_name,
        3: x.bank_acc_no,
        4: x.PositiveAmount,
        5: x.NegativeAmount,
        6: '0.00'
      };
      return result;
    });
    commit('setData', { item: 'exportData', data: result });
  },
  'detailed-bank-report-data' ({ commit }, val) {
    const result = val.map((x, index) => {
      const result = {
        0: index + 1,
        1: x.bank_name,
        2: x.bank_acc_name,
        3: x.bank_acc_no,
        4: x.type,
        5: x.withdraw,
        6: x.deposit,
        7: x.handling_fee,
        8: x.operator_accounts || '--',
        9: x.created_at,
        10: x.note
      };
      return result;
    });
    commit('setData', { item: 'exportData', data: result });
  },
  'member-report-data' ({ commit }, val) {
    const result = val.map((x, index) => {
      const result = {
        0: index + 1,
        1: x.text,
        2: x.total,
        3: x.sumValidbet,
        4: x.sumBet,
        5: x.sumResult,
        6: x.winloss,
        7: '0.00',
        8: '0.00',
        9: '0.00',
        10: x.company,
        11: x.accounted
      };
      return result;
    });
    commit('setData', { item: 'exportData', data: result });
  },
  'member-report-detail-data' ({ commit }, val) {
    const result = val.map((x, index) => {
      const result = {
        0: index + 1,
        1: x.payId,
        2: x.created_at,
        3: x.memberAccount,
        4: '',
        5: x.sumValidbet,
        6: x.sumBet,
        7: x.sumResult,
        8: x.winloss,
        9: '0.00',
        10: '0.00',
        11: '0.00'
      };
      return result;
    });
    commit('setData', { item: 'exportData', data: result });
  },
  'member-statistic-win-data' ({ commit }, val) {
    const result = val.map((x, index) => {
      const result = {
        0: index + 1,
        1: x.memberAccount,
        2: x.total,
        3: x.sumValidbet,
        4: x.sumBet,
        5: x.sumResult,
        6: x.winloss
      };
      return result;
    });
    commit('setData', { item: 'exportData', data: result });
  },
  'member-statistic-lose-data' ({ commit }, val) {
    const result = val.map((x, index) => {
      const result = {
        0: index + 1,
        1: x.memberAccount,
        2: x.total,
        3: x.sumValidbet,
        4: x.sumBet,
        5: x.sumResult,
        6: x.winloss
      };
      return result;
    });
    commit('setData', { item: 'exportData', data: result });
  },
  'member-statistic-notDeposit-data' ({ commit }, val) {
    const result = val.map((x, index) => {
      const result = {
        0: index + 1,
        1: x.account,
        2: x.created_at || '--'
      };
      return result;
    });
    commit('setData', { item: 'exportData', data: result });
  },
  'member-statistic-notLogin-data' ({ commit }, val) {
    const result = val.map((x, index) => {
      const result = {
        0: index + 1,
        1: x.account,
        2: x.created_at || '--'
      };
      return result;
    });
    commit('setData', { item: 'exportData', data: result });
  },
  'member-statistic-deposit-data' ({ commit }, val) {
    const result = val.map((x, index) => {
      const result = {
        0: index + 1,
        1: x.memberAccount,
        2: x.deposit_sumAmount
      };
      return result;
    });
    commit('setData', { item: 'exportData', data: result });
  },
  'member-statistic-withdrawal-data' ({ commit }, val) {
    const result = val.map((x, index) => {
      const result = {
        0: index + 1,
        1: x.memberAccount,
        2: x.withdrawal_sumAmount
      };
      return result;
    });
    commit('setData', { item: 'exportData', data: result });
  },
  'member-statistic-discount-data' ({ commit }, val) {
    const result = val.map((x, index) => {
      const result = {
        0: index + 1,
        1: x.memberAccount,
        2: x.discount_sumAmount
      };
      return result;
    });
    commit('setData', { item: 'exportData', data: result });
  },
  'transaction-statistic-data' ({ commit }, val) {
    const result = val.map((x, index) => {
      const result = {
        0: index + 1,
        1: x.operator_accounts,
        2: x.positive_count || '0',
        3: x.Avg_Deposit_Process_Time,
        4: x.negativ_count || '0',
        5: x.Avg_WIthdraw_Process_Time,
        6: x.promos_count || '0',
        7: x.Avg_Promotion_Process_Time
      };
      return result;
    });
    commit('setData', { item: 'exportData', data: result });
  },
  'player_freq-data' ({ commit }, val) {
    const result = val.map((x, index) => {
      const result = {
        0: index + 1,
        1: x.account,
        2: x.name || '--',
        3: x.phone || '--',
        4: x.email || '--',
        5: x.count
      };
      return result;
    });
    commit('setData', { item: 'exportData', data: result });
  },
  'InactiveCustomerDepositStatistic-data' ({ commit }, val) {
    const result = val.map((x, index) => {
      const result = {
        0: index + 1,
        1: x.name || '--',
        2: x.account,
        3: x.email || '--',
        4: x.phone || '--',
        5: x.members_created_at,
        6: x.members_login_at,
        7: x.deposit_created_at
      };
      return result;
    });
    commit('setData', { item: 'exportData', data: result });
  },
  'agent_performance-data' ({ commit }, val) {
    const result = val.map((x, index) => {
      const result = {
        0: index + 1,
        1: x.agent_code,
        2: x.deposit_amount,
        3: x.withdrawal_amount,
        4: x.discount_amount || 0,
        5: x.Adjustment_deposit_amount,
        6: x.Adjustment_withdrawal_amount,
        7: '0.00',
        8: x.sumValidbet,
        9: x.sumPay,
        10: x.sumResult
      };
      return result;
    });
    commit('setData', { item: 'exportData', data: result });
  },
  'rebate-report-total' ({ commit }, val) {
    const result = {
      0: '',
      1: '',
      2: '總筆數',
      3: val.total + '筆'
    };
    commit('setData', { item: 'exportTotal', data: result });
  },
  'transaction-report-total' ({ commit }, val) {
    const result = {
      0: 'Total',
      1: '',
      2: val.AdjustmentPositiveAmount,
      3: val.AdjustmentNegativeAmount,
      4: val.PositiveAmount,
      5: val.NegativeAmount,
      6: val.DiscountAmount,
      7: '0.00'
    };
    commit('setData', { item: 'exportTotal', data: result });
  },
  'real-time-total' ({ commit }, val) {
    const result = val.map((x) => {
      const result = {
        0: x.text,
        1: '',
        2: '',
        3: '',
        4: '',
        5: '',
        6: '',
        7: '',
        8: '',
        9: '',
        10: '',
        11: x.negativeTotal,
        12: x.positiveTotal,
        13: x.handlingFee,
        14: ''
      };
      return result;
    });
    commit('setData', { item: 'exportTotal', data: result });
  },
  'off-rec-adj-hist-total' ({ commit }, val) {
    commit('setData', { item: 'exportTotal', data: val });
  },
  'bank-report-total' ({ commit }, val) {
    const result = {
      0: 'Total',
      1: '',
      2: '',
      3: '',
      4: val.PositiveAmount,
      5: val.NegativeAmount,
      6: '0.00'
    };
    commit('setData', { item: 'exportTotal', data: result });
  },
  'detailed-bank-report-total' ({ commit }, val) {
    const result = val.map((x) => {
      const result = {
        0: x.text,
        1: '',
        2: '',
        3: '',
        4: '',
        5: x.negativeTotal,
        6: x.positiveTotal,
        7: x.handlingFee,
        8: '',
        9: '',
        10: ''
      };
      return result;
    });
    commit('setData', { item: 'exportTotal', data: result });
  },
  'member-report-total' ({ commit }, val) {
    const result = {
      0: 'Total',
      1: '',
      2: val.total,
      3: val.sumValidbet,
      4: val.sumBet,
      5: val.sumResult,
      6: val.sumWinloss,
      7: '0.00',
      8: '0.00',
      9: '0.00',
      10: val.sumCompany,
      11: val.rate
    };
    commit('setData', { item: 'exportTotal', data: result });
  },
  'member-report-detail-total' ({ commit }, val) {
    const result = {
      0: 'Total',
      1: '',
      2: '',
      3: '',
      4: '',
      5: val.sumValidbet,
      6: val.sumBet,
      7: val.sumResult,
      8: val.sumWinloss,
      9: '0.00',
      10: '0.00',
      11: '0.00'
    };
    commit('setData', { item: 'exportTotal', data: result });
  },
  'member-statistic-win-total' ({ commit }, val) {
    const result = {
      0: 'Total',
      1: '',
      2: val.total,
      3: val.sumValidbet,
      4: val.sumBet,
      5: val.sumResult,
      6: val.sumWinloss
    };
    commit('setData', { item: 'exportTotal', data: result });
  },
  'member-statistic-lose-total' ({ commit }, val) {
    const result = {
      0: 'Total',
      1: '',
      2: val.total,
      3: val.sumValidbet,
      4: val.sumBet,
      5: val.sumResult,
      6: val.sumWinloss
    };
    commit('setData', { item: 'exportTotal', data: result });
  },
  'member-statistic-notDeposit-total' ({ commit }, val) {
    commit('setData', { item: 'exportTotal', data: val });
  },
  'member-statistic-notLogin-total' ({ commit }, val) {
    commit('setData', { item: 'exportTotal', data: val });
  },
  'member-statistic-deposit-total' ({ commit }, val) {
    commit('setData', { item: 'exportTotal', data: val });
  },
  'member-statistic-withdrawal-total' ({ commit }, val) {
    commit('setData', { item: 'exportTotal', data: val });
  },
  'member-statistic-discount-total' ({ commit }, val) {
    commit('setData', { item: 'exportTotal', data: val });
  },
  'transaction-statistic-total' ({ commit }, val) {
    commit('setData', { item: 'exportTotal', data: val });
  },
  'player_freq-total' ({ commit }, val) {
    const result = {
      0: '',
      1: '',
      2: '',
      3: val.totalFrequency,
      4: val.totalPlayer,
      5: val.avgFrequency
    };
    commit('setData', { item: 'exportTotal', data: result });
  },
  'InactiveCustomerDepositStatistic-total' ({ commit }, val) {
    commit('setData', { item: 'exportTotal', data: val });
  },
  'agent_performance-total' ({ commit }, val) {
    const result = {
      0: 'Total',
      1: '',
      2: val.deposit_amount,
      3: val.withdrawal_amount,
      4: val.discount_amount,
      5: val.Adjustment_deposit_amount,
      6: val.Adjustment_withdrawal_amount,
      7: '0.00',
      8: val.sumValidbet,
      9: val.sumPay,
      10: val.sumResult
    };
    commit('setData', { item: 'exportTotal', data: result });
  }
};

export const getters = {
  getField
};
