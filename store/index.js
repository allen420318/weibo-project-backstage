import { getField, updateField } from 'vuex-map-fields';
import vue from 'vue';
export const state = () => ({
  dateTimeArray: [
    // { type: -1, show: '全部' },
    { type: 0, show: '今天' },
    { type: 1, show: '昨天' },
    { type: 6, show: '過去7天' },
    { type: 7, show: '過去30天' },
    { type: 2, show: '本週' },
    { type: 3, show: '上週' },
    { type: 4, show: '本月' },
    { type: 5, show: '上月' }
  ]
  /*
  今天
  昨天
  過去7天
  過去30天
  這個星期
  上個星期
  這個月
  上個月
  */
});
const simpleDateFormat = function (data) {
  if (!data) {
    return;
  }
  const date = new Date(data);
  const year = date.getFullYear();
  const month = (date.getMonth() + 1).toString().padStart(2, 0);
  const day = date.getDate().toString().padStart(2, 0);
  const hour = date.getHours().toString().padStart(2, 0);
  const minute = date.getMinutes().toString().padStart(2, 0);
  const second = date.getSeconds().toString().padStart(2, 0);

  return [year, month, day].join('-') + ' ' + [hour, minute, second].join(':');
};
export const mutations = {
  updateField,
  setData (state, payload) {
    state[payload.item] = payload.data;
  },
  initDateTimeArray (state) {
    const now = new Date();
    for (const i in state.dateTimeArray) {
      switch (state.dateTimeArray[i].type) {
        case 0: {
          const dateTime = simpleDateFormat(now);
          const date = dateTime.split(' ')[0];
          vue.set(state.dateTimeArray[i], 'value', date + ' 00:00:00~' + date + ' 23:59:59');
          break;
        }
        case 1: {
          const dateTime = simpleDateFormat(now - 24 * 60 * 60 * 1000);
          const date = dateTime.split(' ')[0];
          vue.set(state.dateTimeArray[i], 'value', date + ' 00:00:00~' + date + ' 23:59:59');
          break;
        }
        case 2: {
          let week = now.getDay();
          if (week == 0) {
            week = 7;
          }
          let start = simpleDateFormat(now * 1 - (week - 1) * 24 * 60 * 60 * 1000);
          start = start.split(' ')[0] + ' 00:00:00';
          let end = simpleDateFormat(now * 1 - (week - 7) * 24 * 60 * 60 * 1000);
          end = end.split(' ')[0] + ' 23:59:59';
          vue.set(state.dateTimeArray[i], 'value', start + '~' + end);
          break;
        }
        case 3: {
          let week = now.getDay();
          if (week == 0) {
            week = 7;
          }
          let start = simpleDateFormat(now * 1 - (week - 1 + 7) * 24 * 60 * 60 * 1000);
          start = start.split(' ')[0] + ' 00:00:00';
          let end = simpleDateFormat(now * 1 - (week - 7 + 7) * 24 * 60 * 60 * 1000);
          end = end.split(' ')[0] + ' 23:59:59';
          vue.set(state.dateTimeArray[i], 'value', start + '~' + end);
          break;
        }
        case 4: {
          const start = simpleDateFormat(new Date(now.getYear() + 1900, now.getMonth(), 1));
          const end = simpleDateFormat(new Date(now.getYear() + 1900, now.getMonth() + 1, 0, 23, 59, 59));
          vue.set(state.dateTimeArray[i], 'value', start + '~' + end);
          break;
        }
        case 5: {
          const start = simpleDateFormat(new Date(now.getYear() + 1900, now.getMonth() - 1, 1));
          const end = simpleDateFormat(new Date(now.getYear() + 1900, now.getMonth(), 0, 23, 59, 59));
          vue.set(state.dateTimeArray[i], 'value', start + '~' + end);
          break;
          // const d = new Date(2008, month + 1, 0);
        }
        case 6: { // 過去7天
          let start = simpleDateFormat(now * 1 - 7 * 24 * 60 * 60 * 1000);
          start = start.split(' ')[0] + ' 00:00:00';
          let end = simpleDateFormat(now * 1 - 60 * 60 * 1000);
          end = end.split(' ')[0] + ' 23:59:59';
          vue.set(state.dateTimeArray[i], 'value', start + '~' + end);
          break;
        }
        case 7: { // 過去30天
          let start = simpleDateFormat(now * 1 - 30 * 24 * 60 * 60 * 1000);
          start = start.split(' ')[0] + ' 00:00:00';
          let end = simpleDateFormat(now * 1 - 60 * 60 * 1000);
          end = end.split(' ')[0] + ' 23:59:59';
          vue.set(state.dateTimeArray[i], 'value', start + '~' + end);
          break;
        }
      }
    }
  }

};

export const actions = {

};

export const getters = {
  getField
};
