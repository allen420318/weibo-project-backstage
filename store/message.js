import { getField, updateField } from 'vuex-map-fields';
export const state = () => ({
  messageType: 0,
  messageText: '',
  messageOk: null,
  messageCancel: null
});

export const mutations = {
  updateField,
  setData (state, payload) {
    if (payload.ok) {
      state.messageType = 1;
      state.messageText = payload.message;
      state.messageOk = payload.ok;
      state.messageCancel = payload.cancel;
    } else {
      state.messageType = 0;
      state.messageText = payload.message;
    }
  },
  resetData (state) {
    state.messageType = 0;
    state.messageText = '';
    state.messageOk = null;
    state.messageCancel = null;
  }
};

export const actions = {
  setMessage ({ commit }, data) {
    commit('setData', data);
  }
};

export const getters = {
  getField
};
