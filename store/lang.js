import { getField, updateField } from 'vuex-map-fields';
import vue from 'vue';
import { getLangList, getTransList } from '~/api/lang.js';

export const state = () => ({
  langTranslate: {},
  langList: [],
  langObject: {},
  selectLang: ''
});

export const mutations = {
  updateField,
  setData (state, payload) {
    state[payload.item] = payload.data;
  },
  setLangTranslate (state, payload) {
    state.langTranslate[payload.key] || vue.set(state.langTranslate, payload.key, {});
    vue.set(state.langTranslate[payload.key], payload.lang, payload.val);
  }
};
let getLangTimer;
const langKey = [];
export const actions = {
  getLangList ({ commit, state }, func) {
    getLangList().then(({ data }) => {
      const langList = data;
      const langObject = {};
      for (const i in data) {
        const item = data[i];
        langObject[item.lang] = item;
      }
      commit('setData', { item: 'langList', data: langList });
      commit('setData', { item: 'langObject', data: langObject });
      func && func(langList);
    });
  },

  setLangKey ({ commit, state }, val) {
    if (!state.langTranslate[val] || !state.langTranslate[val][state.selectLang]) {
      if (langKey.indexOf(val) == -1 && val !== '') {
        langKey.push(val);
      }
    }

    clearTimeout(getLangTimer);
    getLangTimer = setTimeout(() => {
      if (!langKey.length) { return; }
      getTransList({
        lang: state.selectLang,
        contentList: langKey
      }).then(({ data }) => {
        for (const i in data) {
          const item = data[i];
          commit('setLangTranslate', { lang: state.selectLang, key: item.key, val: item.val });
        }
      });
    }, 500);
  }
};

export const getters = {
  getField
};
