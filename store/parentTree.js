import { getField, updateField } from 'vuex-map-fields';
import { getRoleList, getCommonParent, addCommonParent, updateCommonParent, delCommonParent } from '~/api/admin.js';
export const state = () => ({
  treeList: [],
  tree: {},
  treeParent: {},
  roleList: [],
  nameObject: {},
  dragName: null
});

export const mutations = {
  updateField,
  setData (state, payload) {
    state[payload.item] = payload.data;
  },
  resetData (state, payload) {
    state[payload.item] = null;
  },
  updateTree (state) {
    const data = state.treeList;
    const tree = {};
    const treeParent = {};
    for (const i in data) {
      const { power_id, power, pid, parent_power } = data[i];
      const parent = parent_power + '-' + pid;
      const base = power + '-' + power_id;
      tree[parent] || (tree[parent] = []);
      tree[parent].push(base);
      treeParent[base] = parent;
    }
    state.tree = tree;
    state.treeParent = treeParent;
  },
  addTreeList (state, payload) {
    const index = state.treeList.findIndex((item) => {
      for (const field in payload) {
        if (payload[field] != item[field]) {
          return false;
        }
      }
      return true;
    });
    if (index == -1) {
      state.treeList.push(payload);
      this.commit('parentTree/updateTree');
    }
  },
  delTreeList (state, index) {
    if (index != -1) {
      state.treeList.splice(index, 1);
      this.commit('parentTree/updateTree');
    }
  }
};

export const actions = {
  initData ({ commit, state }) {
    getCommonParent().then(({ data }) => {
      commit('setData', { item: 'treeList', data });
      commit('updateTree');
    });
    getRoleList().then(({ data }) => {
      commit('setData', { item: 'roleList', data });
      const nameObject = {};
      for (const i in data) {
        const { id, name } = data[i];
        nameObject['R-' + id] = name;
      }
      // nameObject['A-0'] = this.getLang('代理');
      // nameObject['S-0'] = 'root';
      commit('setData', { item: 'nameObject', data: nameObject });
    });
  }
};

export const getters = {
  getField
};
