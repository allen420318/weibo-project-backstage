import { getField, updateField } from 'vuex-map-fields';

const statusList = [
  { show: '新要求', value: '0' },
  { show: '處理中(ok)', value: '1' },
  { show: '處理中(不ok)', value: '2' },
  { show: '已被確認', value: '3' },
  { show: '已遭拒', value: '4' }
  // { show: '已修正', value: '5' }
];

const statusObject = {};
for (const i in statusList) {
  const item = statusList[i];
  statusObject[item.value] = item.show;
}

export const state = () => ({
  statusList,
  statusObject
});

export const mutations = {
  updateField,
  setData (state, payload) {
    state[payload.item] = payload.data;
  },
  resetData (state, payload) {
    state[payload.item] = null;
  }
};

export const actions = {

};

export const getters = {
  getField
};
