import { getField, updateField } from 'vuex-map-fields';
export const state = () => ({
  guide: {},
  guideTree: {},
  profile: null,
  power: null,
  nowOrder: 0
});

export const mutations = {
  updateField,
  setData (state, payload) {
    state[payload.item] = payload.data;
  },
  resetData (state, payload) {
    state[payload.item] = null;
  }
};

export const actions = {
  setGuide ({ commit }, guide) {
    commit('setData', { item: 'guide', data: guide });
    const guideTree = {};
    for (const i in guide) {
      const { parent } = guide[i];
      guideTree[parent] || (guideTree[parent] = []);
      guideTree[parent].push(guide[i]);
    }
    commit('setData', { item: 'guideTree', data: guideTree });
  },
  setProfile ({ commit }, val) {
    commit('setData', { item: 'profile', data: val });
  },
  clearProfile ({ commit }) {
    commit('resetData', { item: 'profile' });
  },
  setPower ({ commit }, val) {
    commit('setData', { item: 'power', data: val });
  }
};

export const getters = {
  getField
};
