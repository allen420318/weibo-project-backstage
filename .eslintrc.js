module.exports = {
  root: true,
  env: {
    browser: true,
    node: true
  },
  parserOptions: {
    parser: 'babel-eslint'
  },
  extends: [
    '@nuxtjs',
    'plugin:nuxt/recommended'
  ],
  plugins: [
  ],
  // add your custom rules here
  rules: {
    'vue/require-prop-types': 'off',
    'vue/singleline-html-element-content-newline': 'off',

    'vue/html-self-closing': ['error', {
      html: { normal: 'never', void: 'always' }
    }],
    semi: ['error', 'always'],
    'vue/no-v-html': 'off',
    'no-console': 'off',
    eqeqeq: 'off',
    'no-unused-vars': 'off',
    'unicorn/prefer-includes': 'off',
    'no-lonely-if': 'off',
    camelcase: 'off'
  }
};
