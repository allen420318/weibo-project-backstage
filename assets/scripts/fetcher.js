import axios from 'axios';
import objectFlat from '~/components/objectFlat.js';
axios.defaults.baseURL = process.env.baseUrl;
axios.defaults.headers.common['Content-Type'] = 'application/json';
axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
axios.defaults.withCredentials = true;

export const fetcher = async (APIUrl, httpVerb = 'POST', APIParams, payload) => {
  if (httpVerb.toLowerCase() == 'get') {
    APIParams = objectFlat.encode(APIParams);
  }
  const axiosConfig = {
    method: httpVerb.toLowerCase(),
    url: APIUrl,
    params: APIParams,
    data: payload
  };

  return await axios(axiosConfig)
    .then((response) => {
      if (response.data.status) {
        return response.data;
      } else {
        return Promise.reject(response.data);
      }
    })
    .catch((error) => {
      if (error.response) {
        const errRes = error.response;
        throw errRes.data;
      } else {
        throw error;
      }
    });
};
