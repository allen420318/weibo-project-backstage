if __name__ == '__main__':
    from pprint import pprint
    cs = {}
    for i in open('c.txt', 'r').read().replace(' ', '-').split('\n')[:-1]:
        i = i.split('\t')
        name = i[0].lower()
        code = i[1]
        cs[name] = code
    #pprint(cs)

    import os

    for root, dirs, fname in os.walk('.'):
        for fs in fname:
            fs = fs.replace('.png', '')

            cmd = 'rename "%s.png" "%s.png"' % (
                fs,
                cs[fs.lower()] if fs.lower() in cs else fs.lower()
            )
            print(cmd)
            os.system(cmd)
